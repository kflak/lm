l = LM.new();
l.speakerTest;
l.playVideo;
l.pauseVideo;
l.videoAlpha;
l.videoVolume = 0;
l.videoVolume;
l.fadeVideoAlpha(1, 5);
l.fadeVideoVolume(-21, 5);

~handstand = LMMBGranulator.new.play;
~handstand.reverbMix = 0.1;
~handstand.minRate = 0.5;
~handstand.maxRate = 1.5;
~handstand.minDur = 0.3;
~handstand.maxDur = 1;
~handstand.threshold = 0.02;
~handstand.free;


(
fork{ 
    ~somethingDiff = LMSomethingDifferent.new.play;
    ~somethingDiff.db = -40;
    ~somethingDiff.minRate = 1.5;
    ~somethingDiff.maxRate = 4.0;
    ~somethingDiff.bufArray = LM.buf[\somethingsDifferent]; 
    ~somethingDiff.fadeOutTime = 40;
    ~somethingDiff.freeMBsImmediately = false;
    60.wait;
    ~somethingDiff.free;
    ~coffee = LMMBCoffeeTalk.new.play;
    ~coffee.db = -6;
}
)
~coffee.free;

// Kenneth solo
~granulator = LMMBGranulator.new.play(13, 14, 15, 16);
~granulator.free;

// klli on the snow
~reactiveEq = LMReactiveEq.new.play;
~reactiveEq.cutoff = 228;
~reactiveEq.free;

~train = LMTrainMic.new(inBus: LM.numSpeakers + 4, db: 18);
~train.free;
~snowKlli.mutate = true;















~luulur = LMMBLuulurShuffle.new(threshold: 0.04).play;
~luulur.minRate = 1.0;
~luulur.maxRate = 1.4;
~luulur.free;
~windy = LMMBWindy.new.play;
~windy.free;

// ball?
~foldbass = LMMBFoldBass.new.play;
~foldbass.free;

~stormeye = LMStormEye.new;
~stormeye.db = 23;

