# LM

### Code for the performance Ludus Microtonalis

Code for the performance Ludus Microtonalis by Roosna and Flak (and a bunch of composers)

### Installation

Open up SuperCollider and evaluate the following line of code:
`Quarks.install("https://gitlab.com/kflak/lm")`
