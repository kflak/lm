(
var offsetVideoTime1 = 100;
var offsetVideoTime2 = 115;
var offsetVideoTime3 = 200;
var offsetVideoTime4 = 210;
var offsetVideoTime5 = 215;
~lm = LM.new;
~cuePlayer = CuePlayer.new;
~cuePlayer.blockTrigger(interval: 0.5);

~cuePlayer.add(
    timeline: [
        0, {
            ~lm.playVideo;
            ~lm.videoVolume = -12;
        },
        (4*60+45), {
            ~lm.fadeVideoAlpha(0, 20);
        },
        (5*60+14), {
            "Pause video".postln;
            ~lm.pauseVideo;
            "Handstand begin".postln;
            ~handstand = LMHandstand.new(db: -9).play;
            ~handstand.db = -9;
        },
        (5*60+13+offsetVideoTime1), {
            ~lm.playVideo;
            ~lm.fadeVideoAlpha(1, 0.5);
        },
        (7*60+18+offsetVideoTime1), {
            "Free Kenneth's handstand".postln;
            (13..16).do{|i| ~handstand.freeMB(i) };
        },
        (8*60+8+offsetVideoTime1), {
            ~lm.fadeVideoAlpha(0, 5);
            "Free Külli's handstand".postln;
            ~handstand.free;
        },
        (8*60+13+offsetVideoTime1), {
            "Pause video before Gerhard's piano".postln;
            ~lm.pauseVideo;
        },
        (8*60+13+offsetVideoTime2), {
            "Fade in video for Gerhard's piano".postln;
            ~lm.playVideo;
            ~lm.fadeVideoAlpha(1, 10);
        },
        (10*60+20+offsetVideoTime2), {
            "Start something different mb".postln;
            ~somethingDifferent = LMSomethingDifferent.new.play;
        },
        (11*60+34+offsetVideoTime2), {
            "Fade out video for end of something different".postln;
            ~lm.fadeVideoAlpha(0, 5);
        },
        (11*60+39+offsetVideoTime2), {
            ~lm.fadeVideoVolume(-70, 1);
        },
        (11*60+40+offsetVideoTime2), {
            ~lm.pauseVideo;
        },
        (11*60+35+offsetVideoTime3), {
            "Start coffeetalk".postln;
            ~coffee = LMMBCoffeeTalk.new.play;
            "Restart video. Still muted".postln;
            ~lm.playVideo;
        },
        (12*60+0+offsetVideoTime3), {
            "Fade in video for coffeetalk".postln;
            ~lm.fadeVideoAlpha(1, 10);
        },
        (13*60+0+offsetVideoTime3), {
            "Free something different".postln;
            ~somethingDifferent.free;
        },
        (13*60+20+offsetVideoTime3), {
            "Fade in luulur's coffeetalk in time for Erlich".postln;
            ~lm.fadeVideoVolume(-12, 40);
        },
        (16*60+0+offsetVideoTime3), {
            "Free coffeetalk".postln;
            ~coffee.free;
        },
        (17*60+34+offsetVideoTime3), {
            "Pause video for Kenneth solo".postln;
            ~lm.pauseVideo;
        },
        (17*60+34+offsetVideoTime4), {
            "Kenneth solo".postln;
            ~kennethSolo = LMMBGranulator.new(db: -17).play(13, 14, 15, 16);
            ~lm.playVideo;
        },
        (19*60+35+offsetVideoTime4), {
            "Snowy Külli".postln;
            ~snowKlli = LMReactiveEq.new.play(9, 10, 11, 12);
            ~kennethSolo.free;
        },
        (20*60+36+offsetVideoTime4), {
            "Snowy Külli 1".postln;
            ~snowKlli1 = LMReactiveEq.new(db: -10).play(9, 10, 11, 12);
            ~snowKlli1.mutate = true;
        },
        (21*60+35+offsetVideoTime4), {
            "Free Snowy Külli".postln;
            ~snowKlli.free;
        },
        (22*60+15+offsetVideoTime4), {
            "Kenneth on the closet".postln;
            ~em = LMMBEm.new.play(13, 14, 15, 16);
        },
        (23*60+46+offsetVideoTime4), {
            "Onions frying".postln;
            "Free em".postln;
            ~em.free;
            "Free snowKlli1".postln;
            ~snowKlli1.free;
        },
        (26*60+0+offsetVideoTime4), {
            "Mics on".postln;
            ~mics = LMMics.new;
        },
        (30*60+0+offsetVideoTime4), {
            "Külli hallway solo".postln;
            ~train = [4, 5].collect{
                arg inBus;
                LMTrainMic.new(inBus: LM.numSpeakers + inBus, db: 10) 
            };
        },
        (34*60+40+offsetVideoTime4), {
            "End Külli mic solo".postln;
            ~train.do(_.free);
        },
        (34*60+42+offsetVideoTime4), {
            "Fade video out".postln;
            ~lm.fadeVideoAlpha(0, 1);
        },
        (34*60+44+offsetVideoTime4), {
            "Pause video".postln;
            ~lm.pauseVideo;
            ~lm.fadeVideoVolume(-70, 1);
        },
        (34*60+44+offsetVideoTime5), {
            "Play video".postln;
            ~lm.playVideo;
            ~lm.fadeVideoAlpha(1, 1);
            ~lm.fadeVideoVolume(-12, 1);
        },
        (37*60+55+offsetVideoTime5), {
            "Growl".postln;
            ~foldbass = LMMBFoldBass.new(db: -26).play;
        },
        (40*60+40+offsetVideoTime5), {
            ~lm.fadeVideoAlpha(0, 1);
        },
        (41*60+45+offsetVideoTime5), {
            ~foldbass.free;
        }
    ]
)
)

~cuePlayer.next;
