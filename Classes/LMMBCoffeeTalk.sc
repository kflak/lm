LMMBCoffeeTalk { 

    var <>db = -3;
    var fxChain;
    var synth;
    var task;
    var fadeOutTime = 120;

    *new{ ^super.new.init; }

    init{
        fxChain = FxChain.new(
            level: db.dbamp,
            out: LM.mainBus,
            fadeInTime: 1,
            fadeOutTime: fadeOutTime,
        ).play;

        fxChain.add(\eq, [
            \locut, Pfunc{LM.mbData[14].xbus.getSynchronous.linexp(0, 1, 80, 600)}, 0.1,
        ]);

        fxChain.add(\jpverb,[
            \revtime, 3,
            \mix, Pfunc{LM.mbData[13].xbus.getSynchronous.pow(2) }, 0.1,
        ]);
    }

    play{
        synth = Synth(\playbuf, [
            \buf, LM.buf[\lollid][0],
            \out, fxChain.in,
            \loop, 1,
        ], target: fxChain.group);

        task = TaskProxy({
            inf.do{
                var d = LM.mbData[10].xbus.getSynchronous.linlin(0, 1, -30, db);
                var rate = LM.mbData[9].xbus.getSynchronous;
                rate = rate.linexp(0.0, 1.0, 2, 0.5);
                synth.set(\rate, rate, \amp, d.dbamp);
                0.05.wait;
            };
        }).play;   
    }

    free {
        task.stop;
        synth.release(fadeOutTime);
        fxChain.free;
    }
}

