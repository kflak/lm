LMTest1 : UnitTest {
	test_check_classname {
		var result = LM.new;
		this.assert(result.class == LM);
	}
}


LMTester {
	*new {
		^super.new.init();
	}

	init {
		LMTest1.run;
	}
}
