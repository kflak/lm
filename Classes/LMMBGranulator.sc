LMMBGranulator : LMMBDeltaTrig {
    var <>bufArray;
    var <>minDur = 1;
    var <>maxDur = 5;
    var <reverbTime = 3;
    var <reverbMix = 0.2;
    var <>minRate = 0.25;
    var <>maxRate = 0.5;

    *new{|db= 0, speedlim=0.5, threshold=0.05, minAmp= -10, maxAmp=6, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initLMMBGranulator;
    }

    initLMMBGranulator{
        bufArray = LM.buf[\treeperc];
        this.prAddFx;
    }

    reverbTime_ {|val| fxChain.fx[\jpverb].set(\revtime, val); reverbTime = val}
    reverbMix_ {|val| fxChain.fx[\jpverb].set(\mix, val); reverbMix = val}

    prAddFx {
        fxChain.add(\jpverb, [
            \lag, 5,
            \revtime, reverbTime,
            \mix, reverbMix,
        ]);
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \granulator,
                \dur, dt.linlin(0.0, 1.0, minDur, maxDur),
                \buf, Prand(bufArray),
                \posRate, Pwhite(0.01, 0.4),
                \posRateModFreq, Pexprand(0.1, 3),
                \posRateModDepth, 0.04,
                \overlap, Pwhite(8, 12),
                \tFreq, Pwhite(2, 40),
                \tFreqModFreq, Pexprand(0.1, 1.0),
                \tFreqModDepth, Pexprand(0.1, 1.0),
                \rate, Pwhite(minRate, maxRate, inf),
                \rateModDepth, Pexprand(0.001, 0.1),
                \rateModFreq, Pexprand(0.01, 0.3),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \release, Pkey(\dur),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
