LMSomethingDifferent : LMMBGranulator {

    *new{|db= -40, speedlim=0.5, threshold=0.02, minAmp= -10, maxAmp=6, fadeInTime=90, fadeOutTime=40|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initLMHandstand;
    }

    initLMHandstand {
        minRate = 1.5;
        maxRate = 4.0;
        bufArray = LM.buf[\somethingsDifferent]; 
        freeMBsImmediately = false;
    }
}
