LMMBFoldBass : LMMBDeltaTrig {

    var <>minDuration = 1;
    var <>maxDuration = 4;
    var <reverbMix = 0.1;
    var <reverbTime = 3;

    *new{|db= 0, speedlim=0.5, threshold=0.05, minAmp= -10, maxAmp=6, fadeInTime=90, fadeOutTime=60|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initLMMBFoldBass;
    }

    initLMMBFoldBass{
        this.prAddFx;
        freeMBsImmediately = false;
    }

    reverbTime_ {|val| fxChain.fx[\jpverb].set(\revtime, val); reverbTime = val}
    reverbMix_ {|val| fxChain.fx[\jpverb].set(\mix, val); reverbMix = val}

    prAddFx {
        fxChain.add(\jpverb, [
            \lag, 5,
            \revtime, reverbTime,
            \mix, reverbMix,
        ]);
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            Pfindur( dt.linlin(0, 1, minDuration, maxDuration),
                Pmono(
                    \foldbass,
                    \lag, Pseq([0, Pwhite(0.0, 1.0)]),
                    \dur, Pexprand(0.1, 3),
                    \detune, Pexprand(0.001, 4.0),
                    \db, dt.linlin(0, 1, minAmp, maxAmp),
                    \degree, rrand(-8, 8),
                    \octave, 3,
                    \degree, Pwhite(-12, 12),
                    \attack, 0.1,
                    \release, dt.linlin(0, 1, minDuration, maxDuration),
                    \modFreq, Pexprand(0.1, 1),
                    \hicut1, Pexprand(20, 1000),
                    \hicut2, Pexprand(800, 2000),
                    \minFreq, Pexprand(100, 200),
                    \maxFreq, Pexprand(500, 3000),
                    \out, fxChain.in,
                    \group, fxChain.group,
                )
            ).play;
        }
    }
}
