LMMics {
    var <db;
    var <mics;

    *new{
        arg db = 6;
        ^super.newCopyArgs(db).init;
    }

    db_ {
        arg d;
        db = d;
        mics.do(_.level_(db));
    }

    init{
        mics = [4, 5].collect{
            arg i;
            FxChain.new(
                level: db.dbamp,
                in: LM.numSpeakers + i,
                out: LM.mainBus,
                fadeInTime: 1,
                fadeOutTime: 30,
                numInputBusChannels: 1,
            ).play;
        };

        2.do{
            arg i;
            mics[i].add(\panner, [\pan, 0]);
        };
    }

    free{
        mics.do(_.free);
    }
}
