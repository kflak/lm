LMTrainMic {

    var <inBus;
    var <db;
    var fxChain;
    var server;

    *new{|inBus=3, db=0|
        ^super.newCopyArgs(inBus, db).init;
    }

    db_ {|val| fxChain.level = val.dbamp; db = val}

    init{
        server = LM.server;
        fork{ 
            fxChain = FxChain.new(
                level: db.dbamp,
                in: inBus,
                out: LM.mainBus,
                fadeInTime: 30,
                fadeOutTime: 20,
                numInputBusChannels: 1,
            ).play;

            server.sync;

            fxChain.add(\panner, [
                \pan, 0,
            ]);
            fxChain.add(\eq, [
                \locut, 80,
                \hishelfdb, -3,
                \hishelffreq, 400,
            ]); 

            fxChain.add(\ringMod, [
                \mix, 1,
                \depth, 2,
                \modfreq, 50,
                \amp, -0.dbamp,
            ]); 

            fxChain.add(\ringMod, [
                \mix, 1,
                \depth, 2,
                \modfreq, 20,
                \amp, -0.dbamp,
            ]); 

            fxChain.add(\ringMod, [
                \mix, 1,
                \depth, 3,
                \modfreq, 100,
                \amp, -0.dbamp,
            ]); 

            fxChain.add(\limiter, [
                \limit, -3.dbamp
            ]); 

            fxChain.add(\greyhole, [
                \delayTime, 0.3,
                \feedback, 0.8,
                \mix, 0.6
            ]); 

            fxChain.add(\ringMod, [
                \mix, 0.3,
                \depth, 3,
                \modfreq, Pseg(Pwhite(3, 10), Pwhite(5, 15)), 0.1,
                \amp, -0.dbamp,
            ]); 

            fxChain.add(\eq, [
                \locut, 120,
                \hishelfdb, -12, 
            ]); 

            fxChain.add(\limiter, [
                \limit, -3.dbamp
            ]); 
        }    }

    free {
        fxChain.free;
    }
}


