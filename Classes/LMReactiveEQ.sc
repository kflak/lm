LMReactiveEq : LMMBDeltaTrig {

    var currentPos = 0;
    var initTime;
    var elapsedTime;
    var mutateMags;
    var <mutate = false;

    *new{|db=6, speedlim=0.5, threshold=0.01, minAmp= -20, maxAmp=0, fadeInTime=30, fadeOutTime=60|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initLMReactiveEq;
    }

    initLMReactiveEq{
        initTime = thisThread.seconds;
        elapsedTime = initTime;
        this.prAddFx;
        freeMBsImmediately = false;
    }

    prAddFx {
        fxChain.add(\pvEq);
        fxChain.add(\eq, [
            \hishelffreq, 400,
            \hishelfdb, -12,
        ]);
        fxChain.add(\compressor, [
            \ratio, 8,
            \thresh, -30.dbamp,
            \amp, 12.dbamp,
        ]);
        fxChain.add(\jpverb, [
            \revtime, 5,
            \mix, 0.3,
        ]);
    }

    prSetMags {
        arg cutoff=128;
        var vals = (0..256).collect{|i| 
            ( i > cutoff ).asInteger;
        };
        fxChain.fx[\pvEq].set(\magnitudes, Ref( vals ));
    }
    
    mutate_ {
        arg m = true;
        mutate = m;
        if (mutate){ this.prMutateMags };
    }

    prMutateMags {
        arg cutoff=128, prob=0.5, tick=0.1;
        var vals = (0..256).collect{|i| 
            ( i > cutoff ).asInteger;
        };
        mutateMags = Routine.run{
            var x;
            LM.server.sync;
            inf.do{
                if(mutate){
                    if(prob.coin){
                        x = vals.removeAt(vals.size.rand);
                        vals.insert(vals.size.rand, x);
                    };
                    fxChain.fx[\pvEq].set(\magnitudes, Ref( vals ));
                };
                tick.wait;
            };
        }.play;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|

            var buf = LM.buf[\verkstedshallen].choose;
            var numFrames = buf.numFrames;
            var dur = 0.2;
            var step = dur * LM.server.sampleRate;
            var len = dt.linlin(0.0, 1.0, step, step * 10);
            var pos = (currentPos, currentPos+step..currentPos+len);
            currentPos = pos[pos.size-1].mod(numFrames);
            if(mutate.not){
                this.prSetMags( dt.linlin(0, 1, 256, 0) );
            };

            Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, dur,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 4,
                \startPos, Pseq(pos),
                \legato, 2,
                \rate, Pwhite(0.25, 1.0),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }

    free{
        super.free;
        mutateMags.stop;
    }
}
