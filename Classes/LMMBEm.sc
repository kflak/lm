LMMBEm : LMMBDeltaTrig {
    var scale, pitches;

    *new{|db= 6, speedlim=0.9, threshold=0.04, minAmp=0, maxAmp=15, fadeInTime=2, fadeOutTime=60|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initLMMBEm;
    }

    initLMMBEm {
        scale = Scale.new([0, 4, 8, 9, 13, 17, 21], 22, tuning: \et22, name: "superpyth 7");
        pitches = (scale.size*2).collect{|i| scale.degreeToFreq(i, 442, -1)};
        this.prAddFx;
        freeMBsImmediately = false;
    }

    prAddFx {
        fxChain.add(\autoTune, [
            \pitches, `pitches,
            \mix, 1,
        ]);
        fxChain.add(\greyhole, [
            \mix, 0.6,
            \feedback, 0.6,
            \delayTime, 0.2
        ]); 
        fxChain.add(\compressor, [
            \thresh, -4.dbamp,
            \ratio, 8,
            \amp, 6.dbamp,
        ]); 
        fxChain.add(\jpverb, [
            \revtime, 2,
            \mix, 0.2
        ]);
        fxChain.add(\eq, [
            \hishelfdb, -9,
            \hishelffreq, 500,
            \locut, 80
        ]);
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(LM.buf[\em]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.8,
                \release, Pkey(\dur) * 2,  
                \rate, Pwhite(1, 4),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 40),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
