LMHandstand : LMMBGranulator {

    *new{|db= 0, speedlim=0.5, threshold=0.02, minAmp= -10, maxAmp=6, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initLMHandstand;
    }

    initLMHandstand {
        reverbMix = 0.1;
        minRate = 0.5;
        maxRate = 1.5;
        minDur = 0.3;
        maxDur = 1.5;
    }
}
