LMMBLuulurShuffle : LMMBShuffle {

    var dummy;

    *new{|db= 0, speedlim=0.3, threshold=0.02, minAmp= -6, maxAmp=0, fadeInTime=1, fadeOutTime=60|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initLMMBLuulurShuffle;
    }

    initLMMBLuulurShuffle {
        this.prAddFx;
        buf = LM.buf[\luulurRaamid];
        grainSize = 0.4;
        freeMBsImmediately = false;
        loop = true;
    }

    prAddFx {
        // fxChain.add(\jpverb, [
        //     \revtime, Pwhite(0.1, 4), Pwhite(1, 5),
        //     \mix, 0.3,
        // ]); 
        fxChain.add(\eq, [
            \lag, 1,
            \locut, Pseg(Pwhite(300, 500), Pwhite(1, 4)),
        ]);
    }
}
