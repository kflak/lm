LMStormEye {

    var fxChain;
    var <>inBus = 3;
    var db = 0;

    *new{
        ^super.new.init;
    }

    db_ {|val| fxChain.level = val.dbamp; db = val}

    init {
        fxChain = FxChain.new(
            level: 0.dbamp,
            in: inBus,
            out: LM.mainBus,
            fadeInTime: 30,
            fadeOutTime: 60,
            numInputBusChannels: 1,
        ).play;

        fxChain.add(\panner, [
            \pan, 0,
        ]);

        fxChain.add(\eq, [
            \hishelffreq, 400,
            \hishelfdb, -6,
            \locut, 50,
        ]);

        fxChain.add(\ringMod, [
            \mix, 0.3,
            \depth, 2,
            \modfreq, 50,
            \amp, 0.dbamp,
        ]);

        fxChain.add(\ringMod, [
            \mix, 0.3,
            \depth, 2,
            \modfreq, 20,
            \amp, 0.dbamp,
        ]);

        fxChain.add(\ringMod, [
            \mix, 0.3,
            \depth, 3,
            \modfreq, 50,
            \amp, 0.dbamp,
        ]);

        fxChain.addPar(
            \comb, [\mix, 1, \delay, 0.002, \decay, 2.5, \amp, 1/4],
            \comb, [\mix, 1, \delay, 0.003, \decay, 2.5, \amp, 1/4],
            \comb, [\mix, 1, \delay, 0.005, \decay, 2.5, \amp, 1/4],
        );

        fxChain.add(\greyhole, [
            \delayTime, 0.3,
            \feedback, 0.6,
            \mix, 0.3
        ]);

        fxChain.add(\eq, [
            \locut, 500,
            \hishelfdb, -12,
            \hishelffreq, 700,
            \peakfreq, 450,
            \peakdb, -6,
            \hicut, 900
        ]);

        fxChain.add(\limiter, [
            \limit, -3.dbamp
        ]);
    }

    free{
        fxChain.free;
    }
}
