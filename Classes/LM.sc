LM {
    classvar <>numInputBusChannels = 8;
    classvar <numSpeakers = 2;
    classvar <numSubs = 0;
    classvar <numOutputBusChannels;
    classvar <packageRoot;
    classvar <server, options;
    classvar <mbData;
    classvar <mb = #[9, 10, 11, 12, 13, 14, 15, 16];
    classvar <buf;
    classvar <mainLevel = 1.0;
    classvar <subLevel = 0.316;
    classvar <mainGroup, <mainBus, <main, <sub;

    var videoPort = 12345;
    var <video;
    var <videoVolume = 0;
    var <videoAlpha = 1;

    *new {
        ^super.new.init;
    }

    *initClass{
        server = server ?? { Server.default };
        packageRoot = LM.filenameSymbol.asString.dirname.dirname;
    }

    playVideo {
        video.sendMsg('/setPaused', false);
    }

    pauseVideo {
        video.sendMsg('/setPaused', true);
    }

    videoVolume_ {
        arg v;
        video.sendMsg('/volume', v.dbamp);
        videoVolume = v;
    }

    videoAlpha_ {
        arg a;
        video.sendMsg('/alpha', a);
        videoAlpha = a;
    }

    fadeVideoVolume {
        arg to=0.0, time=1.0, tick=0.02;
        var env = Env.new([videoVolume, to], time).asStream;
        var numTicks = time / tick;
        fork{
            (numTicks+1).do{
                videoVolume = env.next;
                video.sendMsg('/volume', videoVolume.dbamp);
                tick.wait;
            };
        };
    }

    fadeVideoAlpha {
        arg to=1.0, time=1.0, tick=0.03;
        var env = Env.new([videoAlpha, to], time).asStream;
        var numTicks = time / tick;
        fork{
            (numTicks+1).do{
                videoAlpha = env.next;
                video.sendMsg('/alpha', videoAlpha);
                tick.wait;
            };
        };
    }

    init { 

        video = NetAddr.new("localhost", videoPort);
        mbData = IdentityDictionary.new;
        mb.do{|id| mbData.put(id, MBData.new(id))};
        MBDeltaTrig.mbData = mbData;

        options = server.options;
        options.numInputBusChannels = numInputBusChannels;
        numOutputBusChannels = numSpeakers + numSubs;
        options.numOutputBusChannels = numOutputBusChannels;
        FxChain.numSpeakers = numSpeakers;
        server.latency = 0.1;
        server.waitForBoot({
            load(packageRoot+/+"SynthDefs.scd");

            server.sync;

            mainGroup = Group.new;

            mainBus = Bus.audio(server, numSpeakers);
            main = Synth(\route, [
                \in, mainBus, 
                \amp, mainLevel,
                \out, 0
                ], mainGroup);
            if(numSubs > 0){
                sub = Synth(\mono, [
                    \in, mainBus, 
                    \amp, subLevel, 
                    \out, numSpeakers
                    ], mainGroup);
            };
            
            
            LM.loadBuffers(packageRoot+/+"audio");
        });
    }

    *numSpeakers_ {|val|
        numSpeakers = val;
        this.prSetNumOutputBusChannels;
    }

    *numSubs_ {|val|
        numSubs = val;
        this.prSetNumOutputBusChannels;
    }

    *prSetNumOutputBusChannels {
        numOutputBusChannels = numSpeakers + numSubs;
    }

    *mainLevel_ {|level|
        mainLevel = level;
        main.set(\amp, mainLevel);
    }

    *subLevel_ {|level|
        subLevel = level;
        sub.set(\amp, subLevel);
    }

    *loadBuffers {|path|
        server.doWhenBooted({
            var audioDirCount=0;
            buf = Dictionary.new;
            PathName((path).standardizePath).folders.do({|folder|
                buf[folder.folderName.asSymbol] = SoundFile.collectIntoBuffersMono(folder.fullPath+/+"*");
                folder.fullPath.postln;
                audioDirCount = audioDirCount + 1; 
            });
        "% audio directories loaded\n".postf(audioDirCount);
        });
    }

    speakerTest {
        fork{ 
            var dur = 1;
            LM.numSpeakers.do{|i, idx| 
                {Out.ar(
                    idx, PinkNoise.ar() * XLine.kr(0.1, 0.00001, dur, doneAction: 2)
                )}.play;
                dur.wait
            };
            LM.numSubs.do{|i, idx|
                {Out.ar(
                    LM.numSpeakers + i, SinOsc.ar(88) * XLine.kr(0.3, 0.00001, 4, doneAction: 2)
                )}.play;
                1.wait;
            }
        };
    }
}
